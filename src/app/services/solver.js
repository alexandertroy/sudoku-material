services.service('solver', function($timeout) {

    var solver = this;
    var full = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    var rows, cols, grid;

    solver.init = function(_grid) {
        grid = _grid;
        rows = grid.size;
        cols = grid.size;
    };

    solver.solve = function() {
        // add iterations
        solver.rowStrategy();
        solver.colStrategy();
        solver.boxStrategy();
    };

    solver.getCell = function(row, col) {
        return grid.cells[col + (row * 9)]
    };

    solver.writeUserValue = function (cell, value) {
        grid.solvedCells++;
        cell.userValue = value;
        cell.active = true;
        cell.computedPossibleValues = [];

        $timeout(function() {
            cell.active = false;
        }, 2000);

        console.log("set user value: " + cell.userValue);
    };

    solver.trySolveMissing = function(emptyCells, givenValues) {
        var missingValues = [];

        emptyCells.forEach(function(cell) {
            solver.reducePossibilities(cell, givenValues);
            cell.computedPossibleValues.forEach(function (v) {
                if(!missingValues[v]) {
                    missingValues[v] = [];
                }
                missingValues[v].push(cell);
            });
        });

        missingValues.forEach(function(v, i) {
            if(v.length == 1) {
                solver.writeUserValue(v[0], i);
            }
        });
    };

    solver.reducePossibilities = function(cell, givenValues) {

        givenValues = givenValues.map(function (x) {
            return parseInt(x, 10);
        });

        if(cell.computedPossibleValues.length == 0) {
            cell.computedPossibleValues = full.filter(function(val) {
                return givenValues.indexOf(val) === -1;
            });
        } else if(cell.computedPossibleValues.length > 1) {
            cell.computedPossibleValues = cell.computedPossibleValues.filter(function(val) {
                return givenValues.indexOf(val) === -1;
            });
        }
    };

    solver.rowStrategy = function() {

        for (var r = 0; r < rows; r++) {
            var rowValues = [];
            var emptyCells = [];

            for(var c = 0; c < cols; c++) {
                var cell = solver.getCell(r, c);

                if(cell.userValue > 0) {
                    rowValues.push(cell.userValue);
                } else {
                    emptyCells.push(cell);
                }
            }

            solver.trySolveMissing(emptyCells, rowValues);
        }
    };

    solver.colStrategy = function() {

        for (var c = 0; c < cols; c++) {
            var colValues = [];
            var emptyCells = [];

            for(var r = 0; r < rows; r++) {
                var cell = solver.getCell(r, c);

                if(cell.userValue > 0) {
                    colValues.push(cell.userValue);
                } else {
                    emptyCells.push(cell);
                }
            }

            solver.trySolveMissing(emptyCells, colValues);
        }
    };

    solver.boxStrategy = function() {

        var boxMax = 3;
        var boxGridMax = grid.size - boxMax;

        for (var bc = 0; bc <= boxGridMax; bc += 3) {
            for (var br = 0; br <= boxGridMax; br += 3) {
                var boxValues = [];
                var emptyCells = [];

                for(var c = 0; c < boxMax; c++) {
                    for(var r = 0; r < boxMax; r++) {
                        var cell = solver.getCell(r + br, c + bc);

                        if(cell.userValue > 0) {
                            boxValues.push(cell.userValue);
                        } else {
                            emptyCells.push(cell);
                        }
                    }
                }

                solver.trySolveMissing(emptyCells, boxValues);
            }
        }

    };

    return solver;
});