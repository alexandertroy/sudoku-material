services.service('generator', function() {

    var generator = this;
    var grid;

    generator.init = function(_grid) {
        grid = _grid;
    };

    generator.gridUserSetValue = function(row, col, value) {
        grid.cells[col + (row * 9)].userValue = value;
        grid.cells[col + (row * 9)].locked = true;
        grid.solvedCells++;
    };

    generator.createPuzzle = function(difficulty) {

        if(difficulty == 3) {
            generator.gridUserSetValue(0, 2, 7);
            generator.gridUserSetValue(0, 5, 1);
            generator.gridUserSetValue(0, 6, 8);
            generator.gridUserSetValue(0, 7, 3);

            generator.gridUserSetValue(1, 1, 5);
            generator.gridUserSetValue(1, 4, 8);
            generator.gridUserSetValue(1, 7, 6);
            generator.gridUserSetValue(1, 8, 2);

            generator.gridUserSetValue(2, 5, 6);
            generator.gridUserSetValue(2, 8, 5);

            generator.gridUserSetValue(3, 0, 5);
            generator.gridUserSetValue(3, 5, 4);

            generator.gridUserSetValue(4, 1, 9);
            generator.gridUserSetValue(4, 4, 5);
            generator.gridUserSetValue(4, 7, 1);

            generator.gridUserSetValue(5, 3, 6);
            generator.gridUserSetValue(5, 8, 3);

            generator.gridUserSetValue(6, 0, 1);
            generator.gridUserSetValue(6, 3, 7);

            generator.gridUserSetValue(7, 0, 7);
            generator.gridUserSetValue(7, 1, 8);
            generator.gridUserSetValue(7, 4, 3);
            generator.gridUserSetValue(7, 7, 5);

            generator.gridUserSetValue(8, 1, 6);
            generator.gridUserSetValue(8, 2, 2);
            generator.gridUserSetValue(8, 3, 8);
            generator.gridUserSetValue(8, 6, 4);

        } else {
            generator.gridUserSetValue(0, 1, 5);
            generator.gridUserSetValue(0, 2, 4);
            generator.gridUserSetValue(0, 4, 6);
            generator.gridUserSetValue(0, 8, 2);

            generator.gridUserSetValue(1, 4, 9);
            generator.gridUserSetValue(1, 5, 1);
            generator.gridUserSetValue(1, 6, 6);
            generator.gridUserSetValue(1, 8, 5);

            generator.gridUserSetValue(2, 1, 9);
            generator.gridUserSetValue(2, 2, 6);
            generator.gridUserSetValue(2, 5, 2);
            generator.gridUserSetValue(2, 7, 3);
            generator.gridUserSetValue(2, 8, 8);

            generator.gridUserSetValue(3, 1, 6);
            generator.gridUserSetValue(3, 5, 5);
            generator.gridUserSetValue(3, 6, 2);
            generator.gridUserSetValue(3, 8, 7);

            generator.gridUserSetValue(4, 1, 3);
            generator.gridUserSetValue(4, 7, 6);

            generator.gridUserSetValue(5, 0, 7);
            generator.gridUserSetValue(5, 2, 9);
            generator.gridUserSetValue(5, 3, 8);
            generator.gridUserSetValue(5, 7, 5);

            generator.gridUserSetValue(6, 0, 8);
            generator.gridUserSetValue(6, 1, 7);
            generator.gridUserSetValue(6, 3, 6);
            generator.gridUserSetValue(6, 6, 9);
            generator.gridUserSetValue(6, 7, 2);

            generator.gridUserSetValue(7, 0, 9);
            generator.gridUserSetValue(7, 2, 3);
            generator.gridUserSetValue(7, 3, 2);
            generator.gridUserSetValue(7, 4, 8);

            generator.gridUserSetValue(8, 0, 6);
            generator.gridUserSetValue(8, 4, 7);
            generator.gridUserSetValue(8, 6, 8);
            generator.gridUserSetValue(8, 7, 1);
        }
        
    };

    return generator;
});