var directives = angular.module('sudoku.directives', []);


directives.directive('grid', function() {
    return {
        restrict: 'E',
        templateUrl: "app/views/grid.html",
        scope: {
            config: "="
        },
        controller: function ($scope) {

        }
    };
});

directives.directive('cell', function() {
    return {
        restrict: 'E',
        templateUrl: "app/views/cell.html",
        scope: {
            row: "=",
            col: "="
        },
        controller: function ($scope) {

            $scope.cell = $scope.$root.$$childHead.grid.cells[$scope.col + ($scope.row * 9)];
            $scope.cell.row = $scope.row;
            $scope.cell.col = $scope.col;

            $scope.focused = function() {
                //$scope.cell.cross = !$scope.cell.cross;
            };
        }
    };
});

var filters = angular.module('sudoku.filters', []);


filters.filter("increment", function() {

    return function(i) {
        console.log(i);
        return ++i;
    };
});