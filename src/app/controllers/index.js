var controllers = angular.module('sudoku.controllers', []);

controllers.controller('AppCtrl', function($scope, $rootScope, solver, generator) {

    var config = {
        crosshair: false,
        iterationsNeeded: 0
    };

    var size = 9;
    var max = size * size;
    $scope.max = max;

    $scope.grid = {
        size: size,
        cells: [],
        solvedCells: 0,
        rows: [0, 1, 2, 3, 4, 5, 6, 7, 8],
        cols: [0, 1, 2, 3, 4, 5, 6, 7, 8]
    };

    for (var i = 0; i < max; i++) {
        $scope.grid.cells.push({
            userValue: null,
            userPossibleValues: [],
            computedPossibleValues: [],
            active: false,
            cross: false,
            locked: false
        });
    }

    generator.init($scope.grid);
    solver.init($scope.grid);

    $scope.solveGrid = function() {
        solver.solve();
    };

    $scope.testGrid = function() {
        generator.createPuzzle(2);
    };

    $scope.testGridHard = function() {
        generator.createPuzzle(3);
    };

    $scope.resetGrid = function() {

        $scope.grid.solvedCells = 0;

        for (var i = 0; i < max; i++) {
            var cell = $scope.grid.cells[i];
            cell.userValue = null;
            cell.userPossibleValues = [];
            cell.computedPossibleValues = [];
            cell.locked = false;
        }
    };

    $scope.testGrid();
});